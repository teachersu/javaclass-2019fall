# Java Class (2019 Fall)

[JavaFX Tutorial](http://tutorials.jenkov.com/javafx/index.html)

[Examples](http://www.naturalprogramming.com/javagui/javafx/)

[Animation and Visual Effects in JavaFX](https://docs.oracle.com/javase/8/javafx/get-started-tutorial/animation.htm)

[Animation basic](https://docs.oracle.com/javase/8/javafx/visual-effects-tutorial/basics.htm)

[Timeline Java Docs](https://docs.oracle.com/javase/8/javafx/api/javafx/animation/Timeline.html)
## 2020-03-05
#### Homework
Using `setInitialDirectory` method of FileChooser to make FileChooser to start from your home directory. It is same as `setInitialDirectory` as opening folder from home directory.

#### Escape Characters
We discussed '\\`, "\r", "\n", and "\t" in class.


## 2020-02-28
#### Player Status

[Player Status Java Doc](https://docs.oracle.com/javafx/2/api/javafx/scene/media/MediaPlayer.Status.html)

```
player.statusProperty().addListener((ov) -> {
	System.out.println("Player status: " + player.getStatus());
	//System.out.println(player.getTotalDuration().toMinutes());

});
```

#### Interact with time length slider

Update timelength when the video is playing.

```
player.currentTimeProperty().addListener((ov) -> {
	timeLength.setValue(player.currentTimeProperty().getValue().toMillis()
			/ player.getTotalDuration().toMillis() * 100);
});
```

Play video from time which is the timelength value.

```
timeLength.valueProperty().addListener((ov) -> {
	if (timeLength.isPressed()) {
		player.seek(player.getTotalDuration().multiply(timeLength.getValue() / 100));
	}
});
```

#### Interact with Volume slider

Set volume when media player is ready.

```
player.statusProperty().addListener((ov) -> {
	if (player.getStatus() == MediaPlayer.Status.READY) {
		volBar.setValue(player.getVolume() * 100);
	}
});

```

Change volume

```
volBar.valueProperty().addListener((ov) -> {
	if (volBar.isPressed()) {
		player.setVolume(volBar.getValue() / 100);
	}
});

```

## 2020-02-22
Implement the play button.

```
Button playBtn = (Button) evt.getSource();
if (player != null) {
	if (player.getStatus() == MediaPlayer.Status.PLAYING) {
		player.pause();
		playBtn.setText(">");
	} else {
		player.play();
		playBtn.setText("||");
	}
}
```
## 2020-02-18
#### Homework tips
* Add menuItem under `File` menu and give a method name with `OnAction` event.
* Implement the method in `MovieSkillzzController.java`. Refer method `openMovieFile()`.
* Other example code at <http://tutorials.jenkov.com/javafx/directorychooser.html>

#### Binding MovieView properties with Scene properties
In order to keep movie view always full fill scene area, the movie view size needs to be updated when scene size is changed. 

```
DoubleProperty width = movieView.fitWidthProperty();
DoubleProperty height = movieView.fitHeightProperty();
width.bind(Bindings.selectDouble(movieView.sceneProperty(), "width"));
height.bind(Bindings.selectDouble(movieView.sceneProperty(), "height")
		.subtract(menuBar.getHeight() + hBox.getHeight()));

```


## 2020-02-07
#### Homework
In JavaFX, there is another component called `DirectoryChooser`. Add a `MenuItem`, which is called `Select Directory`, under `File` menu and implement it to print out which directory is select. You can use the `getAbsolutePath()` to print the directory name.

#### Resize Label with computed size

* Set the `pref Width` as `USE_COMPUTED_SIZE`
* Using code

```	
	aboutLabel.setPrefWidth(Region.USE_COMPUTED_SIZE);
```

#### Open File
* Open MovieSkillzz.fxml with Scene Builder
* Define method `openMovieFile` for `Open` menuitem
* Using FileChooser to open a file

```	
	// Initial a FileChooser
	FileChooser fileChooser = new FileChooser();
	
	// Show FileChooser
	fileChooser.showOpenDialog(parent);
	
	// Initial start folder
	fileChooser.setInitialDirectory(new File("/"));
	
	// File Chooser Filter
	fileChooser.getExtensionFilters()
					.add(new FileChooser.ExtensionFilter("Media Files", 
					     Arrays.asList("*.mp4", "*.AVI", "*.flv", "*.mkv", "*.mov", "*.wma", "mpg")));
```
					     
* Supported media types

[JavaFx Supported Media Types](https://docs.oracle.com/javafx/2/api/javafx/scene/media/package-summary.html#SupportedMediaTypes)

* Using Media, MediaPlay and MediaView to play video

```	
	File movieFile = fileChooser.showOpenDialog(parent);
	if (movieFile != null) {
		System.out.println(movieFile.toURI().toURL().toExternalForm());
		Media media;
		media = new Media(movieFile.toURI().toURL().toExternalForm());
		MediaPlayer player = new MediaPlayer(media);
		player.play();
	} else {
		System.out.println("You don't choose any file.");
	}
```

* Handle Exception

```
	try {
		...
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}		
```

## 2020-01-31
#### Homework (optional)
Run you application using command line.

*Tips: "Run Configuration..." --> "Show Command Line"*

#### File systems
Disk needs to be partitioned before it can be used. File systems is created using `format` command on Windows. Windows supports FAT (File Allocation Table), FAT32, and NTFS. Most windows are using NTFS now.

![Disk partitions](images/disk-partitions.PNG)

#### Image on alert window

```
// show image
Image image = new Image("icons8-movie-96.png");
ImageView aboutImage = (ImageView) scene.lookup("#aboutImage"); // Cast
aboutImage.setImage(image);
// Change image size
aboutImage.setFitHeight(image.getHeight());
aboutImage.setFitWidth(image.getWidth());
```

## 2020-01-17
Each window has one *fxml* file and one *controller*. If a window controller needs to edit element in another window, it can load the window's fxml file and use `lookup` method to find the element which needs to be accessed. Source codes are stored in the controller which opens other windows. See folloing figure.

![controllers and windows](images/class-2020-01-17.png) 

#### Class path and file systems (tree structure)
To check path using command line tool.

* On windows in command prompt window:

```
echo %path%
```

* On iOS in terminal:

```
echo $PATH
```

* Java class path can help JVM find specify class quickly. Java class path can be folders or jar files

* System path can help operating systems to find commands and shell scripts quickly.

* File systems help uses to organize files in folders and folder also can have other folders. It likes a tree structure.

![File systems tree](images/file-systems-tree.png) 

This is the file systems on C drive on windows. The `c:\` is root. Gray background items are folders (branch). Green background items are files (leaf).

## 2020-01-10
#### Home work
Change the label text in aboutskillzz window using programming.

#### Setting label and image codes
```
Label aboutLabel = (Label) scene.lookup("#aboutLabel");
aboutLabel.setText("AboutLable is Good. Test long centense.");
Image aboutImage = new Image("icons8-movie-96.png");
ImageView aboutImageView = (ImageView) scene.lookup("#aboutImage");
System.out.println(aboutImage.getWidth() + "/" + aboutImage.getHeight());
//aboutImageView.setFitHeight(aboutImage.getHeight());
//aboutImageView.setFitWidth(aboutImage.getWidth());
aboutImageView.setImage(aboutImage);
```

#### Alert buttonTypes
```
ButtonType myButton = new ButtonType("Click Me!");
Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "test", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL, myButton);
String text = myButton.getText();
ButtonType btnType = mybutton.getButtonData();
```

## 2020-01-04
#### Homework
Using confirm alert before close about window.

#### close about window

```
aboutButton.getScene().getWindow().hide();
```

Note: aboutButton also can be accessed as following code.

```
Button button = (Button) evt.getSource();
```
![Window hierarchy](images/class-2020-01-04.png)
#### Set label and image
Because the open function is in different controller, it can not access the items in AboutSkillzzController. We have to use `lookup` function to find the item we want to modify.

```
Label aboutLabel = (Label) root.lookup("#aboutLabel");
aboutLabel.setText("I am an awesome application.");
```

For image, we need to assign the image filename.

```
Image image = new Image("icons8-movie-96.png");
ImageView imageView = (ImageView) root.lookup("#aboutImage");
imageView.setImage(image);
```

## 2019-12-23
Create AboutSkillz Window with fxml file. Next class will implement image, label, and close functions.

## 2019-12-13
#### Homework
Change the about dialog from confirmation to information.

## 2019-12-07
#### Homework
Try to add play button, time slide bar and volume slide bar in HBox container as below.

![hbox](images/20191207-gui.jpg)

## 2019-11-22
* Java Concurrency API (in package [java.util.concurrent](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/package-summary.html))
* [ExecutorService](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html)
* [Future](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Future.html)

## 2019-11-16
Declare a function which has two parameters (String and int) and returns int.

Declare a function which name is withdraw, returns type is int, parameters are accountName(string) and value (int).

#### Homework
Revise the class [LambdaThread.java](MediaPlayer/src/threadings/LambdaThread.java) to make each thread prints the hello string one hundred times.

## 2019-11-09
* How to create a thread?
    * Extend Thread class [ExtendThread1.java](MediaPlayer/src/threadings/ExtendThread1.java)
    * Implement Runnable interface [RunnableThread2.java](MediaPlayer/src/threadings/RunnableThread2.java)
* What is difference between start and run methods in Thread class?
    * `run` method is called directly by current thread. It doesn't start the thread which 
    has been created.
    * `start` method is used to start a thread. When the `start` method is called, the 
    JVM knows that the thread is ready to start and will schedule cpu resource for it. 
    The thread will call `run` method when it has cpu resource.

![Difference between start and run method in Thread class](images/20191109-Thread_run_start.JPG)

## 2019-10-18 and 2019-10-31

### Why Concurrency
Problems:
* I/O bound
* CPU bound

| I/O-Bound Process | CPU-Bound Process |
| --- | --- |
| Your program spends most of its time talking to a slow device, like a network connection, a hard drive, or a printer.	| You program spends most of its time doing CPU operations. |
| Speeding it up involves overlapping the times spent waiting for these devices.	| Speeding it up involves finding ways to do more computations in the same amount of time. |

### Multi-threading
Solve the I/O-Bound problem.

[Runnable interface](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/Runnable.html)

##### Create thread
* Runnable interface
* Inherent Thread class

### Multi-processing
Solve the CPU-Bound problem. It's more complicate because we need to split our problem and reduce the outputs.

## 2019-10-11
### Homework
Refer the addition/subtraction implementation, write your own arithmetic operations multiplication and division using lambda expression.

[ArithmeticOperation.java](MediaPlayer/src/ch5/ArithmeticOperation.java)

[MainArithmetic.java](MediaPlayer/src/ch5/MainArithmetic.java)


### Functional Interfaces
* Only has one abstract method declared.
* Default and static methods are not counted.

[Reception.java](MediaPlayer/src/ch5/Reception.java)

[MainReception.java](MediaPlayer/src/ch5/MainReception.java)

![Functional Interfaces](images/20191011-functional-interfaces.jpg)


### Anonymous classes
* No class name
* Declare and Initial instance at same time

Ex.

```
Talktive dogRule = new Talktive() {
	public void talk() {
		Sstem.out.println("Bark! Bark-bark!");
	}
};
```
![Anonymous classes](images/20191011-anonymous-classes.jpg)

### Lambda Expressions

* Declare an the interface that has only one abstract method.
* Make sure that the arguments of your lambda expression match the arguments of the abstract method.
* Make sure that the return value of your lambda expression matches the return value of the abstract method.

Ex.

```
ArithmeticOperation addition = (first, second)-> {
	return first + second;
}
```
![Lambda Expressions](images/20191011-lambda-expressions.jpg)


## 2019-10-04
### Interface
In Java 11, static method in an Interface is *public*. It can be accessed by any class.

Example structure in book:

![example](images/20191004-interface.JPG)

## 2019-09-28
### Interface

#### Interface features:
* An interface cannot be instantiated.
* An interface does not contain any constructors.
* All of the methods in an interface are abstract.
* An interface cannot contain instance fields. The only fields that can appear in an interface must be declared both static and final.
* An interface is not extended by a class; it is implemented by a class.
* An interface can extend multiple interfaces.
* An interface can have *default* method
* An interface can have *static* method

#### Keyword static
Instance variables are always associated with a object which are created by JVM. It has its own memory address to store its value.

If a instance variable has defined with modifier *static*, it will use shared memory in JVM all time. If one class changed the value, other classes can see the change too.
![keyword static](images/20190928-static.JPG)

#### File extension name (suffix name)
Operation Systems use files's extension name to determine which application can open the file. When a user double click on a file, the OS will start an application to open the file automatically if it knows the file type base on the extension name.

Here some examples:
![file extension name](images/20190928-suffix-name.JPG)

#### Interface structure
```
public interface A {
    variables
    methods
}
```
Variables in interface always have modifiers *public static final*.

#### Keyword default
From Java 8, interface can implement method with *default* key word.
![interface structure](images/20190928-interface-structure.JPG)

#### Home work
Copy code from section 5.1 (not include default method) and prepare to present them 
next class.
Files should be copied:
* Talkative.java
* Swimmable.java
* Fish.java
* Dog.java (The seond one which implements Talkative and Swimmable)
* Cat.java
* Parrot.java

## 2019-09-21
### Review
[Java basic knowledge](https://gitlab.com/teachersu/javaclass2018/blob/20190201/resources/notes.md)

[Java FX note](https://gitlab.com/teachersu/javaclass2018-fx/blob/20190525/MyCaclculator/src/resources/notes.md)

### Basic Knowledge
* Primitive types
* function block
* if else block
* switch block
* Array
* for loop
* class structure
* java package
* ArrayList and Set
* Dictionary (HashTable, HashMap)
* Java Casting
* XML

