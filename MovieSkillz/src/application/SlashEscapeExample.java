package application;

public class SlashEscapeExample {

	public static void main(String[] args) {
		// ABC\CDE
		String str = "ABC\\CDE";
		print(str);
		// ABC"CDE
		str = "ABC\"CDE";
		print(str);
		// \r \n \t
		print("\r abc");
		print("\n abc");
		print("\r\n \\r\\n abc");
		print("\tabc");
		print("22345678abc");
		print("abc\tcde\tefg");
		print("abcef\tcdehi\tefg");
		print("abcefcdehi\tefg");
		print("");
		print("abc        cde        efg");
		print("abc\r\ncde\r\nefg");
	}
	
	public static void print(String str) {
		System.out.println(str);
	}
}
