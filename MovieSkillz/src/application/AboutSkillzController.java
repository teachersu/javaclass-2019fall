package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class AboutSkillzController {

	@FXML
	ImageView aboutImage;

	@FXML
	Label aboutLabel;

	@FXML
	Button aboutButton;

	public void closeAbout(ActionEvent evt) {
//		Button aButton = (Button) evt.getSource();
		// get the aboutButton
		// aboutButton.getScene().getWindow().hide();
		// aButton.getScene().getWindow().hide();
//		System.out.println("Hide window via Image item");
//		aboutImage.getScene().getWindow().hide();
		
		// Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure?", ButtonType.YES, ButtonType.NO);
		ButtonType yesButton = new ButtonType("Yes, please.");
		ButtonType nopeButton = new ButtonType("Nope");
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure?", yesButton, nopeButton);
		alert.setTitle("About MoiveSkillzz");
//		alert.setHeaderText("I am header");
		alert.setHeaderText(null);
//		alert.setContentText("Moiveskillzz is amazing. Do you really want to close me???");
		alert.showAndWait();
		ButtonType result = alert.getResult();
//		if (result.equals(ButtonType.YES)) {
		if (result.equals(yesButton)) {
			aboutImage.getScene().getWindow().hide();
			System.out.println("You clicked 'ok'");
		} else {
			System.out.println("You clicked 'cancel'");
		}

	}

}
