package application;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class MovieSkillzzController {

	@FXML
	MenuItem mAbout;

	@FXML
	MenuItem mAboutSkillz;

	@FXML
	Button aboutButton;

	@FXML
	MediaView movieView;

	@FXML
	MenuBar menuBar;

	@FXML
	HBox hBox;

	@FXML
	Slider timeLength;

	@FXML
	Slider volBar;

	MediaPlayer player;

	public void openMovieFile(ActionEvent evt) {
		MenuItem openMenu = (MenuItem) evt.getSource();
		Window parent = openMenu.getParentPopup().getScene().getWindow();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(new File("C:/Users/navysu/Documents/"));
		File movieFile = fileChooser.showOpenDialog(parent);
		if (movieFile != null) {
			System.out.println(movieFile.getAbsolutePath());
			try {
				System.out.println(movieFile.toURI().toURL().toExternalForm());
				Media media = new Media(movieFile.toURI().toURL().toExternalForm());
				player = new MediaPlayer(media);
				movieView.setMediaPlayer(player);
				DoubleProperty width = movieView.fitWidthProperty();
				DoubleProperty height = movieView.fitHeightProperty();
				width.bind(Bindings.selectDouble(movieView.sceneProperty(), "width"));
				height.bind(Bindings.selectDouble(movieView.sceneProperty(), "height")
						.subtract(menuBar.getHeight() + hBox.getHeight()));
				movieView.setPreserveRatio(true);
//				System.out.println(player.getTotalDuration().toMinutes());
				// player.play();
				player.currentTimeProperty().addListener((ov) -> {
					// System.out.println(player.getTotalDuration().toMinutes());
					timeLength.setValue(player.currentTimeProperty().getValue().toMillis()
							/ player.getTotalDuration().toMillis() * 100);
				});
				player.statusProperty().addListener((ov) -> {
					System.out.println("Player status: " + player.getStatus());
					// System.out.println(player.getTotalDuration().toMinutes());
					if (player.getStatus() == MediaPlayer.Status.READY) {
						volBar.setValue(player.getVolume() * 100);
					}

				});
				timeLength.valueProperty().addListener((ov) -> {
					if (timeLength.isPressed()) {
						player.seek(player.getTotalDuration().multiply(timeLength.getValue() / 100));
					}
				});
				volBar.valueProperty().addListener((ov) -> {
					if (volBar.isPressed()) {
						player.setVolume(volBar.getValue() / 100);
					}
				});
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("You clicked the cancel button");
		}
	}

	public void playVid(ActionEvent evt) {
		Button playBtn = (Button) evt.getSource();
		if (player != null) {
			if (player.getStatus() == MediaPlayer.Status.PLAYING) {
				player.pause();
				playBtn.setText(">");
			} else {
				player.play();
				playBtn.setText("||");
			}
		}
	}

	public void openFolder(ActionEvent evt) {
		MenuItem openFolderMenu = (MenuItem) evt.getSource();
		Window parent = openFolderMenu.getParentPopup().getScene().getWindow();
		DirectoryChooser folderChooser = new DirectoryChooser();
		folderChooser.setInitialDirectory(new File("C:/Users/navysu/Documents/"));
		File directory = folderChooser.showDialog(parent);
		if (directory == null) {
			System.out.println("You clicked the cancel button");
		} else {
			System.out.println("You choosed folder: " + directory.getAbsolutePath());
		}
	}

	public void showAboutSkillz(ActionEvent evt) {
		try {
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("AboutSkillz.fxml"));
			Scene scene = new Scene(root, 300, 200);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			Stage stageAbout = new Stage();
			Label aboutLabel = (Label) scene.lookup("#aboutLabel"); // Cast
			aboutLabel.setText("I am changed by the program!!");
			aboutLabel.setPrefWidth(Region.USE_COMPUTED_SIZE);
			// show image
			Image image = new Image("icons8-movie-96.png");
			ImageView aboutImage = (ImageView) scene.lookup("#aboutImage"); // Cast
			aboutImage.setImage(image);
			// Change image size
			aboutImage.setFitHeight(image.getHeight());
			aboutImage.setFitWidth(image.getWidth());
			stageAbout.setScene(scene);
			if (aboutButton == null) {
				System.out.println("About button doesn't exist.");
			} else {
				System.out.println("About button text is '" + aboutButton.getText() + "'");
			}
			stageAbout.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showAbout(ActionEvent evt) {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("About MoiveSkillzz");
//		alert.setHeaderText("I am header");
		alert.setHeaderText(null);
//		alert.setContentText("Moiveskillzz is amazing. Do you really want to close me???");
		alert.showAndWait();
		ButtonType result = alert.getResult();
		if (result.equals(ButtonType.OK)) {
			System.out.println("You clicked 'ok'");
		} else {
			System.out.println("You clicked 'cancel'");
		}
//		Alert alert2 = new Alert(Alert.AlertType.WARNING);
//		alert2.setTitle("About MoiveSkillzz  22222");
////		alert.setHeaderText("I am header");
//		alert2.setHeaderText(null);
//		alert2.setContentText("Moiveskillzz is amazing.");
//		alert2.showAndWait();
		try {
			Window parent = mAbout.getParentPopup().getScene().getWindow();
			if (parent == null) {
				System.out.println("Cannot get parent");
			} else {
				System.out.println("Parent is OK");
			}
			Stage abtStage = new Stage();
			abtStage.initOwner(parent);
			abtStage.initModality(Modality.WINDOW_MODAL);
			BorderPane abtPane = (BorderPane) FXMLLoader.load(getClass().getResource("About.fxml"));
			Label abtTxt = (Label) abtPane.lookup("#abtTxt");
			if (abtTxt != null) {
				abtTxt.setText("MovieSkillzz ABOUT!");
			}
			// Image image = new
			// Image(getClass().getResource("icons8-movie-96.png").toExternalForm());
			Image image = new Image("icons8-movie-96.png");
			ImageView abtImage = (ImageView) abtPane.lookup("#abtImage");
			if (abtImage != null) {
				abtImage.setImage(image);
			}
			Scene abtScene = new Scene(abtPane, 300, 200);
			abtStage.setScene(abtScene);
			abtStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
