package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class AboutController {
	
	@FXML
	ImageView abtImage;
  
	@FXML
	Label abtTxt;
	
	public void closeAbout(ActionEvent evt) {
		Button clsButton = (Button)evt.getSource();
		clsButton.getScene().getWindow().hide();
		
	}
}
