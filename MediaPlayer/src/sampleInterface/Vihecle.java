package sampleInterface;

public interface Vihecle {
    void changeGear(int a); 
    void speedUp(int a); 
    void applyBrakes(int a); 
    static void display() {
    	System.out.println("I am a static method");
    }
}
