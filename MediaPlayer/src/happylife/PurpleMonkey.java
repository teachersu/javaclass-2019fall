package happylife;

public class PurpleMonkey implements Monkey {
	
	String name = "PurpleMonkey";

	@Override
	public void talk(String word) {
		System.out.println(name + " said: " + word);
	}

	@Override
	public void run(int distance) {
		// TODO Auto-generated method stub
		
	}

	public void sleep() {
		
	}
}
