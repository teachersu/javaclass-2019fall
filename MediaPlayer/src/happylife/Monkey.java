package happylife;

public interface Monkey {
	void talk(String word);
	void run(int distance);
}
