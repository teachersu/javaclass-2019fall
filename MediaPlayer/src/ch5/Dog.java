package ch5;

public class Dog extends Pet implements Talkative, Swimmable {

	@Override
	public void swim(int howFar) {
		if (Swimmable.isSummer()) {
			System.out.println("Will swim about a half of this distance: " + howFar / 2 + " feet");
		} else {
			System.out.println("It's too cold. I don't want to swim.");
		}
	}

//	@Override
//	public void dive(int howDeep) {
//		System.out.println("C'mon, don't ask a dog to dive, please!");
//	}

	@Override
	public void talk() {
		System.out.println("Bark! Bark-bark!");
	}

	@Override
	public void jump() {
		// TODO Auto-generated method stub
		
	}

}
