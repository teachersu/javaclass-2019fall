package ch5;

public class Fish implements Swimmable {

	@Override
	public void swim(int howFar) {
		System.out.println("OK, will swim " + howFar + " feet");
	}

	@Override
	public void dive(int howDeep) {
		System.out.println("OK, will dive " + howDeep + " feet");
	}
	
}
