package ch5;

public interface ArithmeticOperation {
	public double performOperation(double first, double second);
}
