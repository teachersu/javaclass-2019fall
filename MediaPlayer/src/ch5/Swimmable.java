package ch5;

import java.time.LocalDate;
import java.time.Month;

public interface Swimmable {
	public void swim(int howFar); 
	public default void dive (int howDeep) {
		System.out.println("C'mon, I cannot dive.");
	}
	
	static boolean isSummer() {
		Month month = LocalDate.now().getMonth();
		if (month == Month.JUNE || month == Month.JULY || month == Month.AUGUST) {
			return true;
		} else {
			return false;
		}
	}
}
