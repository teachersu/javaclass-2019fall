package ch5;

public class Cat implements Talkative {

	@Override
	public void talk() {
		System.out.println("Meow! Meow!");
	}

}
