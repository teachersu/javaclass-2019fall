package ch5;

public interface Movable {

	public String moveDistance(int distance);
}
