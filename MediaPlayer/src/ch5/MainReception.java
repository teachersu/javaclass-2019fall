package ch5;

public class MainReception {

	public static void main(String[] args) {
		Reception hotelReception = new Reception() {

			@Override
			public void greeting(String name) {
				System.out.println("Hello Mr./Ms. " + name + ", welcome to our hotel.");
			}
		};

		Reception waitress = new Reception() {

			@Override
			public void greeting(String name) {
				System.out.println("Hello Mr./Ms. " + name + ", welcome to our restaurant. My name is NObody. I will serve you.");
			}
		};
		
		hotelReception.greeting("Tom");
		waitress.greeting("Alice");
		
		Reception securityGuard = (name) -> {
			System.out.println("Hello Mr./Ms. " + name +", please show me your photo ID.");
		};
		
		securityGuard.greeting("Bob");
	}

}
