package ch5;

public abstract class Pet {

	public void play() {
		System.out.println("I am playing");
	}
	
	public abstract void jump();
}
