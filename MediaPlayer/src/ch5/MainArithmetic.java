package ch5;

public class MainArithmetic {

	public static void main(String[] args) {
		ArithmeticOperation addition = new ArithmeticOperation() {

			@Override
			public double performOperation(double first, double second) {
				return first + second;
			}
			
		};
		ArithmeticOperation subtraction = new ArithmeticOperation() {

			@Override
			public double performOperation(double first, double second) {
				return first - second;
			}
		};
		calc(addition, 5.0, 9.0);
		calc(subtraction, 9.0, 5.0);
		
		ArithmeticOperation addition1 = (first, second) -> {
			return first + second;
		};
		ArithmeticOperation subtraction1 = (first, second) -> {
			return first - second;
		};
		calc(addition1, 6.0, 9.0);
		calc(subtraction1, 9.0, 3.0);
		
	}
	
	public static void calc(ArithmeticOperation operation, double first, double second) {
		System.out.println("result = " + operation.performOperation(first, second));
	}

}
