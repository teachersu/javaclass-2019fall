package threadings;

import java.util.concurrent.*;

public class ExecutorThreadSample {

	public static void main(String[] args) {
		try {
			ExecutorService executor = Executors.newSingleThreadExecutor();
			Future<?> task0 = executor.submit(() -> {
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String threadName = Thread.currentThread().getName();
				System.out.println("Hello " + threadName);
			});
			System.out.println("Task0 done? " + task0.isDone());
			
			ExecutorService executor2 = Executors.newFixedThreadPool(5);
			// callable
			Future<Integer> task1 = executor2.submit(()-> {
				System.out.println("Start task1...");
				int sum = 0;
				for (int i = 0; i < 1000; i++) {
					sum += i;
				}
				System.out.println("Task1: I have the result: " + sum);
				return sum;
			});
			Future<String> task2 = executor2.submit(()-> {
				System.out.println("Start task2...");
				int sum = 0;
				for (int i = 0; i < 6000; i++) {
					sum += i;
				}
				System.out.println("Task2: I have the result: " + sum);
				return "My result is " + sum;
			});
			
			System.out.println("I am before tasks");
			System.out.println("task0 done" + task0.get());
			System.out.println("task1 = " + task1.get(5, TimeUnit.SECONDS));
			System.out.println("task2 = " + task2.get(5, TimeUnit.SECONDS));
			System.out.println("I am after tasks");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
