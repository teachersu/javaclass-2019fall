package threadings;

import java.util.Scanner;

public class ExampleCPUBound {

	public static void main(String[] args) {
		Thread myThread = new Thread(() -> {
			ExampleCPUBound.printSum();
		});
		Thread myThread2 = new Thread(() -> {
			ExampleCPUBound.printNumbers();
		});	
		myThread.start();
		myThread2.start();
		printName();
	}

	public static void printName() {
		try (Scanner myObj = new Scanner(System.in)) {
			String userName;

			// Enter username and press Enter
			System.out.print("Enter username: ");
			userName = myObj.nextLine();

			System.out.println("Username is: " + userName);
		}
	}
	
	public static void printNumbers() {
		for (int i = 0; i < 500; i++) {
			System.out.println("This is number " + i);
		}
	}
	
	public static void printSum() {
		int sum = 0;
		for (int i = 1; i <= 100; i++) {
			sum += i;
		}
		System.out.println("Sum (1 to 10): " + sum);
	}
}
