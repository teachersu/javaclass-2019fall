package threadings;

public class ExtendThread extends Thread {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 100; i++) {
			System.out.println("I am a thread which inhirents Thread, this is loop " + i);
		}
	}
	
	public static void main(String[] args) {
		Thread t1 = new ExtendThread();
		t1.start();
		for (int i = 0; i < 100; i++) {
			System.out.println("I am the main Thread, this is loop " + i);
		}
		
	}
}
