package threadings;

public class RunnableThread1 {
	public static void main(String[] args) {

		Thread myThread = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 5; i++) {
					System.out.println("The value2 of i in power of 4 is " + i * i * i * i * i);
				}
			}
		});
		Thread myThread2 = new Thread(() -> {
			for (int i = 0; i < 5; i++) {
				System.out.println("The value2 of i in power of 4 is " + i * i * i * i * i);
			}
		});

		myThread.start();
		myThread2.start();

		for (int k = 0; k < 5; k++) {
			System.out.println("*** The value of k is " + k + "!");
		}
	}
}
