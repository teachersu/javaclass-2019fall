package threadings;

public class RunnableThread2 implements Runnable {
	
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i=0; i < 1000; i++) {
			System.out.println(Thread.currentThread().getName() + "'s Number: " + i);
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Runnable r1 = new RunnableThread2();
		Runnable r2 = new RunnableThread2();
		Thread t1 = new Thread(r1, "t1");
		Thread t2 = new Thread(r2, "t2");
		t1.start();
		t2.start();
		r1.run();
	}

}
