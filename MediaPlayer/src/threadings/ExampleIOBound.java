package threadings;

import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.*;
import java.util.*;

public class ExampleIOBound {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		downloadAll();
		System.out.println(System.currentTimeMillis() - start);
		start = System.currentTimeMillis();
		downloadAllThreading();
		System.out.println(System.currentTimeMillis() - start);
	}

	public static String downloadWebPage(String url) {
		URL pagelink;
		try {
			pagelink = new URL(url);

			try (BufferedReader in = new BufferedReader(new InputStreamReader(pagelink.openStream()))) {
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					// Do something here
					// System.out.println(inputLine);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url;
	}

	public static void downloadAll() {
		String[] urls = new String[] { "https://www.google.com", "https://www.msn.com", "https://www.yahoo.com",
				"https://www.oracle.com", };
		for (String url : urls) {
			System.out.println(downloadWebPage(url));
		}
	}

	public static void downloadAllThreading() {
		ExecutorService executor = Executors.newWorkStealingPool();

		List<Callable<String>> callables = Arrays.asList(() -> ExampleIOBound.downloadWebPage("https://www.google.com"),
				() -> ExampleIOBound.downloadWebPage("https://www.msn.com"),
				() -> ExampleIOBound.downloadWebPage("https://www.yahoo.com"),
				() -> ExampleIOBound.downloadWebPage("https://www.oracle.com"));
		try {
			executor.invokeAll(callables).stream().map(future -> {
				try {
					return future.get();
				} catch (Exception e) {
					throw new IllegalStateException(e);
				}
			}).forEach(System.out::println);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
