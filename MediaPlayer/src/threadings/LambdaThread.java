package threadings;

public class LambdaThread {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Runnable sayhello = () -> {
			for (int index = 0; index < 100; index++) { 
				System.out.println(Thread.currentThread().getName() + " Says Hello world.");
			}
		};

		Thread t1 = new Thread(sayhello, "t1");
		Thread t2 = new Thread(sayhello, "t2");
		t1.start();
		t2.start();
		Thread t3 = new Thread(() -> {
			for (int j = 0; j < 100; j++) {
				System.out.println(Thread.currentThread().getName() + " says that I am confused.");
			}
		}, "t3");
		t3.start();
		sayhello.run();
	}
}

