package threadings;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorThread {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Runnable sayhello = () -> {
			for (int index = 0; index < 100; index++) { 
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName() + " Says Hello world.");
			}
		};
/*		
		ExecutorService service = Executors.newSingleThreadExecutor();
		service.submit(sayhello);
		sayhello.run();
		service.shutdown();
*/
		ExecutorService service2 = Executors.newFixedThreadPool(2);
		long start = System.currentTimeMillis();
		Future<?> t1 = service2.submit(sayhello);
		Future<?> t2 = service2.submit(sayhello);
		try {
			t1.get();
			t2.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Total time (millis seconds): " + (System.currentTimeMillis() - start));

		service2.shutdown();
		
		
	}

}
