package games;
/*
   DrawingDemoFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-10 File created.
   2015-03-26 Last modification.

   This program demonstrates drawing operations on Canvas.
*/


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.* ;
import javafx.stage.Stage;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;


public class DrawingDemoFX extends Application
{
   public void start( Stage stage )
   {
      stage.setTitle( "DrawingDemoFX.java" ) ;

      Canvas canvas = new Canvas( 896, 512 ) ;
 
      StackPane root_pane = new StackPane() ;

      root_pane.getChildren().addAll( canvas ) ;

      Scene scene = new Scene( root_pane, 896, 512 ) ;

      GraphicsContext context = canvas.getGraphicsContext2D();

      context.strokeText( "Canvas size: " + (int) canvas.getWidth()
                        + "x" + (int) canvas.getHeight(), 20, 20 ) ;


      context.setStroke( Color.BLUE );
      context.setLineWidth( 3 ) ;

      context.strokeLine( 64, 128, 512, 128 ) ; // Horizontal line

      context.setFill( Color.CYAN ) ;

      context.fillRect( 64, 192, 128, 128 ) ;  // Filled square with size 128x128

      context.strokeRect( 256, 192, 148, 128 ) ;  // Hollow rectangle 148x128
      context.setFill( Color.MAGENTA ) ;
      context.fillRect( 266, 202, 128, 108 ) ;    // Filled rectangle 128x108

      // Next we'll draw a ball with radius 64 points.
      // The center point of the ball is (512, 256)

      context.setFill( Color.YELLOW ) ;
      context.fillOval( 448, 192, 128, 128 ) ;
      context.strokeOval( 448, 192, 128, 128 ) ;

      context.setFill( Color.LIGHTGRAY ) ;
      context.setStroke( Color.BLACK );

      // The following statements create a 'Pacman'.
      context.fillArc(   640, 192, 128, 128,  45, 270, ArcType.ROUND ) ;
      context.strokeArc( 640, 192, 128, 128,  45, 270, ArcType.ROUND ) ;

      // The following statements create the 'missing part' of the Pacman.
      context.fillArc(   704, 192, 128, 128, 315,  90, ArcType.ROUND ) ;
      context.strokeArc( 704, 192, 128, 128, 315,  90, ArcType.ROUND ) ;

      context.strokeLine( 512, 384, 768, 384 ) ; // Horizontal line


      stage.setScene( scene ) ;
      stage.show();
   }


   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}


