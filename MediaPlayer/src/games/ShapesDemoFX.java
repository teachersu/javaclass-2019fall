package games;
/*
   ShapesDemoFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-29 File created.
   2015-09-28 Last modification.

   This program demonstrates the use of classes Text, Line, Rectangle, 
   Circle, and Arc. These are all sublasses of class Shape.
*/


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.text.Text ;
import javafx.stage.Stage; 
import javafx.scene.shape.* ;
import javafx.scene.paint.Color;


public class ShapesDemoFX extends Application
{
   public void start( Stage stage )
   {
      stage.setTitle( "ShapesDemoFX.java" ) ;

      Group group_for_shapes = new Group() ;

      Scene scene = new Scene( group_for_shapes, 896, 512 ) ;

      scene.setFill( Color.LIGHTYELLOW ) ;

      Text scene_size_text = new Text( 20, 20,
                                       "Scene size is " + (int) scene.getWidth()
                                     + " x " + (int) scene.getHeight() ) ;

      Line blue_horizontal_line = new Line( 64, 128, 512, 128 ) ;
      blue_horizontal_line.setStroke( Color.BLUE ) ;
      blue_horizontal_line.setStrokeWidth( 3 ) ;

      Rectangle cyan_square = new Rectangle( 64, 192, 128, 128 ) ; 
      cyan_square.setFill( Color.CYAN ) ;

      Rectangle magenta_rectangle = new Rectangle( 266, 202, 128, 108 ) ;
      magenta_rectangle.setFill( Color.MAGENTA ) ;

      Rectangle frame_around_magenta_rectangle =
                                    new Rectangle( 256, 192, 148, 128 ) ;
      frame_around_magenta_rectangle.setFill( Color.TRANSPARENT ) ;
      frame_around_magenta_rectangle.setStroke( Color.BLUE ) ;
      frame_around_magenta_rectangle.setStrokeWidth( 3 ) ;


      Circle yellow_ball = new Circle( 512, 256, // center point (512, 256)
                                       64,       // radius is 64 points
                                       Color.YELLOW ) ;
      yellow_ball.setStroke( Color.BLUE ) ;
      yellow_ball.setStrokeWidth( 3 ) ;


      Arc pacman_shape = new Arc( 704, 256, // center point
                                   64,  64, // width and height radiuses
                                   45,      // start angle in degrees
                                   270 ) ;  // length angle in degrees
      pacman_shape.setType( ArcType.ROUND ) ;
      pacman_shape.setFill( Color.LIGHTGREY ) ;
      pacman_shape.setStroke( Color.BLACK ) ;
      pacman_shape.setStrokeWidth( 3 ) ;

      // The 'pie slice' represents the 'missing' part of pacman shape

      Arc pie_slice = new Arc( 768, 256, 64, 64, 315, 90 ) ;
      pie_slice.setType( ArcType.ROUND ) ;
      pie_slice.setFill( Color.LIGHTGREY ) ;
      pie_slice.setStroke( Color.BLACK ) ;
      pie_slice.setStrokeWidth( 3 ) ;

      Line black_horizontal_line = new Line( 512, 384, 768, 384 ) ;
      black_horizontal_line.setStroke( Color.BLACK ) ;
      black_horizontal_line.setStrokeWidth( 3 ) ;

      // Finally, we'll add all the shapes into the Group, After this they will
      // become visible on the screen.

      group_for_shapes.getChildren().addAll( scene_size_text,
                                             blue_horizontal_line,
                                             cyan_square, magenta_rectangle,
                                             frame_around_magenta_rectangle,
                                             yellow_ball,
                                             pacman_shape, pie_slice,
                                             black_horizontal_line ) ;

      stage.setScene( scene ) ;
      stage.show();
   }


   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}

/* NOTES:

The following links were useful when this program was developed:

http://docs.oracle.com/javase/8/javafx/api/javafx/scene/shape/Shape.html

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/shape/Rectangle.html

http://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html
*/

