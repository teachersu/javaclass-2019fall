package games;
/*
   SinglePictureFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-22 File created.
   2014-12-29 Last modification.


   This program demonstrates
      - how a picture, i.e., an Image object, can be shown in various ways
      - how to use classes Image, ImageView, and Rectangle2D

   When you run this program the used .jpg file must be in the same folder
   where this program is located.

*/


import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Rectangle2D;


public class SinglePictureFX extends Application
{
   public void start( Stage stage )
   {
      Group group_for_imageviews = new Group() ;

      stage.setTitle( "SinglePictureFX.java" ) ;

      Scene scene = new Scene( group_for_imageviews, 960, 744 ) ;

      scene.setFill( Color.LIGHTYELLOW ) ;

      Image picture_to_show = new Image( "gauguin_two_women_on_the_beach_1891.jpg" ) ;

      ImageView natural_size_view = new ImageView( picture_to_show ) ;

      natural_size_view.setX( 20 ) ;
      natural_size_view.setY( 20 ) ;

      ImageView reduced_size_view = new ImageView( picture_to_show ) ;

      reduced_size_view.setX( 20 + picture_to_show.getWidth() + 20 ) ;
      reduced_size_view.setY( 20 ) ;
      reduced_size_view.setFitWidth( picture_to_show.getWidth() / 2 ) ;
      reduced_size_view.setPreserveRatio( true ) ;


      ImageView right_half_view = new ImageView( picture_to_show ) ;

      right_half_view.setX( 20 + picture_to_show.getWidth() + 20 ) ;
      right_half_view.setY( 20 + picture_to_show.getHeight() / 2 + 20 ) ;

      // With the following rectangle we specify that we'll view only the
      // right half of the image.

      Rectangle2D viewport_rectangle =
                       new Rectangle2D( picture_to_show.getWidth() / 2,
                                        0,
                                        picture_to_show.getWidth() / 2,
                                        picture_to_show.getHeight() ) ;

      right_half_view.setViewport( viewport_rectangle ) ;

      group_for_imageviews.getChildren().addAll( natural_size_view,
                                                 reduced_size_view,
                                                 right_half_view ) ;
           
      stage.setScene( scene ) ;
      stage.show() ;

   }

   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}


/*  NOTES:

Thanks for Paul Gauguin for the painting the picture of which we are using here.


The following were useful pages when I was developing this program:

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/image/Image.html

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/image/ImageView.html

*/
