package games;
/*
   FillTransitionDemoFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-15 File created.
   2014-12-29 Last modification.

*/


import javafx.animation.* ;
import javafx.util.Duration;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;


public class FillTransitionDemoFX extends Application
{
   public void start( Stage stage )
   {
      Group group_for_ball = new Group() ;

      stage.setTitle( "FillTransitionDemoFX.java" ) ;

      Scene scene = new Scene( group_for_ball, 600, 500 ) ;

      scene.setFill( Color.LIGHTYELLOW ) ;

      Circle blinking_ball = new Circle( 300, 250, 64, Color.TRANSPARENT ) ;

      group_for_ball.getChildren().add( blinking_ball ) ;
           
      stage.setScene( scene ) ;
      stage.show() ;

      FillTransition fill_transition =
                       new FillTransition( Duration.millis( 4000 ),
                                           blinking_ball,
                                           Color.CYAN,
                                           Color.TRANSPARENT ) ;
      fill_transition.setCycleCount( Transition.INDEFINITE ) ;
      //fill_transition.setAutoReverse( true ) ;
 
      fill_transition.play() ;
   }

   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}
