package games;

/*
   BrickWallFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-22 File created.
   2014-12-29 Last modification.


   This program demonstrates how to create many Rectangle objects
   that will form a kind of brick wall.

*/


import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.Rectangle ;


public class BrickWallFX extends Application
{
   public void start( Stage stage )
   {
      Group group_for_bricks = new Group() ;

      stage.setTitle( "BrickWallFX.java" ) ;

      Scene scene = new Scene( group_for_bricks, 960, 720 ) ;

      scene.setFill( Color.ORANGE ) ;


      int brick_position_x  =  0 ;
      int brick_position_y  =  0 ;

      int brick_height  =   28 ;
      int brick_length  =  112 ;
      int gap_between_bricks =  4 ;
      int row_counter = 0 ;

      while ( brick_position_y  <  scene.getHeight() )
      {
         while ( brick_position_x < scene.getWidth() )
         {
            Rectangle new_brick =
                        new Rectangle( brick_position_x, brick_position_y,
                                       brick_length, brick_height ) ;

            new_brick.setFill( Color.FIREBRICK ) ;

            group_for_bricks.getChildren().add( new_brick ) ;

            brick_position_x = brick_position_x
                             + brick_length + gap_between_bricks ;
         }

         // Now we must prepare to do the next row of bricks.
         // Every second row must start with a half brick.
         row_counter ++ ;

         if ( row_counter % 2 == 1 )
         {
            // This row will start with a half brick.

            brick_position_x = - brick_length / 2 ;
         }
         else
         {
            brick_position_x = 0 ;
         }

         brick_position_y = brick_position_y + brick_height + gap_between_bricks ;

      }
           
      stage.setScene( scene ) ;
      stage.show() ;

   }

   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}


/*  NOTES:


The following were useful pages in developing this program:

http://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/shape/Rectangle.html

*/
