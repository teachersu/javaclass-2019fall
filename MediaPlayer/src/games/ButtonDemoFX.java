package games;

/*
   ButtonDemoFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-08 File created.
   2014-12-29 Last modification.

   This program is a simple example of a Java FX application.

   The UI consists of a Label and a Button. By pressing the button
   the user can change the text of the label.

*/


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button ;
import javafx.scene.control.Label ;
import javafx.scene.layout.VBox ;
import javafx.stage.Stage;
import javafx.geometry.Pos;

public class ButtonDemoFX extends Application
{
   public void start( Stage given_stage )
   {
      Label text_label = new Label( "Hello!" ) ;

      Button button_to_press = new Button( "Press" ) ;


      button_to_press.setOnAction( ( ActionEvent event ) ->
      {
         if ( text_label.getText().equals( "Hello!" ) )
         {
            text_label.setText( "Well done!" ) ;
         }
         else
         {
            text_label.setText( "Hello!" ) ;
         }
         
      } ) ;
        
      VBox root_pane = new VBox();

      root_pane.getChildren().addAll( text_label, button_to_press ) ;

      root_pane.setAlignment( Pos.CENTER ) ; // The VBox is centered
      root_pane.setSpacing( 16 ) ; // Put some space between the Label and Button


      Scene scene = new Scene( root_pane, 300, 250 ) ;

      given_stage.setTitle( "ButtonDemoFX.java" ) ;
      given_stage.setScene( scene ) ;
      given_stage.show();
   }

   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}


/**
   JDK 1.8 or later is required to compile this program.
   
   USEFUL LINKS related to Java FX:
   
   http://docs.oracle.com/javase/8/javafx/get-started-tutorial/hello_world.htm

   http://docs.oracle.com/javase/8/javafx/layout-tutorial/builtin_layouts.htm#JFXLY102


   // This is an alternative way to react to the Button press:

      button_to_press.setOnAction( new EventHandler<ActionEvent>()
      {
         public void handle(ActionEvent event)
         {
            if ( text_label.getText().equals( "Hello!" ) )
            {
               text_label.setText( "Well done!" ) ;
            }
            else
            {
               text_label.setText( "Hello!" ) ;
            }
         }
      } ) ;
**/
