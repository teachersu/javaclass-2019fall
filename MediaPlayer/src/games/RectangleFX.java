package games;
/*
   RectangleFX.java Copyright (c) Kari Laitinen

   http://www.naturalprogramming.com/

   2014-12-27 File created.
   2014-12-27 Last modification.


   This program displays a rectangle whose size can be
   adjusted with a ScrollBar. The color of the rectangle can be
   chosen with RadioButtons.

   This program demonstrates:

     - the use of classes RadioButton and ScrollBar
     - how to assign a lambda expression as a value for an
       object reference

*/


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.* ;
import javafx.scene.control.* ; // RadioButton, ScrollBar

import javafx.scene.layout.* ;
import javafx.stage.Stage;
import javafx.geometry.* ; // Pos, Insets

import javafx.scene.paint.Color;

import javafx.scene.shape.Rectangle ;



public class RectangleFX extends Application
{
   public void start( Stage stage )
   {
      stage.setTitle( "RectangleFX.java" ) ;

      StackPane stack_pane = new StackPane() ;

      Scene scene = new Scene( stack_pane, 600, 480 ) ;

      Rectangle rectangle_on_screen = new Rectangle( scene.getWidth() / 4,
                                                     scene.getHeight() / 4,
                                                     scene.getWidth() / 2,
                                                     scene.getHeight() / 2 ) ;
      // With the following statement we disble the automatic layout
      // management of the object.
      rectangle_on_screen.setManaged( false ) ;

      rectangle_on_screen.setFill( Color.CYAN ) ;
      rectangle_on_screen.setStroke( Color.FIREBRICK ) ; // red/brown outline
      rectangle_on_screen.setStrokeWidth( 4 ) ;


      ScrollBar height_scrollbar = new ScrollBar() ;

      height_scrollbar.setOrientation( Orientation.VERTICAL ) ;
      height_scrollbar.setValue( rectangle_on_screen.getHeight() ) ;
      height_scrollbar.setBlockIncrement( 10 ) ;
      height_scrollbar.setMin( 10 ) ;
      height_scrollbar.setMax( scene.getHeight() ) ;

      ToggleGroup color_selection_buttons = new ToggleGroup() ;

      RadioButton cyan_button       =  new RadioButton( "cyan" ) ;
      RadioButton orchid_button     =  new RadioButton( "orchid" ) ;
      RadioButton green_button      =  new RadioButton( "green" ) ;
      RadioButton lightGray_button  =  new RadioButton( "lightGray" ) ;

      cyan_button.setToggleGroup( color_selection_buttons ) ;
      orchid_button.setToggleGroup( color_selection_buttons ) ;
      green_button.setToggleGroup( color_selection_buttons ) ;
      lightGray_button.setToggleGroup( color_selection_buttons ) ;

      cyan_button.setSelected( true ) ;

      // In the following statement a lambda expression is assigned as
      // a value for an object reference.

      EventHandler<ActionEvent> color_change_handler = (ActionEvent event ) ->
      {
         if ( cyan_button.isSelected() )
         {
            rectangle_on_screen.setFill( Color.CYAN ) ;
         }
         else if ( orchid_button.isSelected() )
         {
            rectangle_on_screen.setFill( Color.ORCHID ) ;
         }
         else if ( green_button.isSelected() )
         {
            rectangle_on_screen.setFill( Color.LIGHTGREEN ) ;
         }
         else if ( lightGray_button.isSelected() )
         {
            rectangle_on_screen.setFill( Color.LIGHTGRAY ) ;
         }
      } ;

      cyan_button.setOnAction( color_change_handler ) ;
      orchid_button.setOnAction( color_change_handler ) ;
      green_button.setOnAction( color_change_handler ) ;
      lightGray_button.setOnAction( color_change_handler ) ;


      // Next, we'll specify what will be done when the ScrollBar is used.

      height_scrollbar.valueProperty().addListener(

         ( observable_value, value, new_value ) ->
         {
            double width_height_ratio = rectangle_on_screen.getWidth() /
                                          rectangle_on_screen.getHeight() ;

            rectangle_on_screen.setHeight( height_scrollbar.getValue() ) ;

            // We'll adjust the width so that the rectangle preserves its
            // original 'beauty'.

            rectangle_on_screen.setWidth( height_scrollbar.getValue() *
                                                     width_height_ratio ) ;

            // The next statements ensure that the rectangle stays in the
            // middle of the Scene.

            rectangle_on_screen.setX( scene.getWidth() / 2 -
                                        rectangle_on_screen.getWidth() / 2 ) ;
            rectangle_on_screen.setY( scene.getHeight() / 2 -
                                        rectangle_on_screen.getHeight() / 2 ) ;
         }
      ) ;

      HBox pane_for_radio_buttons = new HBox();

      pane_for_radio_buttons.getChildren().addAll(
            cyan_button, orchid_button, green_button, lightGray_button ) ;

      pane_for_radio_buttons.setAlignment( Pos.CENTER ) ; // The Box is centered
      pane_for_radio_buttons.setSpacing( 16 ) ; // Put some space between buttons

      // With an Insets object we can specify empty space around the HBox.
      // There will be 20 pixels padding below the HBox.
      pane_for_radio_buttons.setPadding( new Insets( 0, 0, 20, 0 ) ) ;


      BorderPane border_pane = new BorderPane() ;

      border_pane.setRight( height_scrollbar ) ;
      border_pane.setBottom( pane_for_radio_buttons ) ;

      stack_pane.getChildren().addAll( rectangle_on_screen, border_pane ) ;

      stage.setScene( scene ) ;
      stage.show();
   }


   public static void main( String[] command_line_parameters )
   {
      launch( command_line_parameters ) ;
   }
}

/* NOTES:

The following links were useful when this program was developed:

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/shape/Rectangle.html

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/RadioButton.html

https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/ScrollBar.html


*/