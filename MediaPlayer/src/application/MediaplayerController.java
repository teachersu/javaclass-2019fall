package application;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class MediaplayerController {

	@FXML
	HBox hBox;

	@FXML
	MediaView mediaView;

	@FXML
	MenuItem mOpen;

	@FXML
	MenuItem mExit;

	@FXML
	MenuItem mAbout;

	@FXML
	MenuItem mAlert;

	@FXML
	HBox navBox;

	@FXML
	Button btnPlay;

	@FXML
	Slider pTime;

	@FXML
	Slider pVol;

	@FXML
	BorderPane mPane;

	@FXML
	MenuBar mBar;

	MediaPlayer player;

	public void openFile(ActionEvent evt) {
		// find the primary stage
		MenuItem source = (MenuItem) evt.getSource();
		Window theStage = source.getParentPopup().getScene().getWindow();

		// Choose file
		try {
			FileChooser fileChooser = new FileChooser();
			List<String> mediaFileFilter = new ArrayList<String>();
			mediaFileFilter.add("*.avi");
			mediaFileFilter.add("*.mp4");
			mediaFileFilter.add("*.flv");
			mediaFileFilter.add("*.mpg");
			mediaFileFilter.add("*.vob");
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Media Files", mediaFileFilter));
			File file = fileChooser.showOpenDialog(theStage);
			if (file != null) {
				System.out.println(file.toURI().toURL().toExternalForm());
				Media media = new Media(file.toURI().toURL().toExternalForm());
				player = new MediaPlayer(media);
				mediaView.setMediaPlayer(player);
				// full fill the view
				DoubleProperty width = mediaView.fitWidthProperty();
				DoubleProperty height = mediaView.fitHeightProperty();

				width.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
				height.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height")
						.subtract(btnPlay.getHeight() + mBar.getHeight()));
				mediaView.setPreserveRatio(true);

				// Set volume slider
				pVol.setValue(player.getVolume() * 100);

				// Providing functionality to time slider
				player.currentTimeProperty().addListener((ov) -> {
					updatesValues();
				});

				// In order to jump to the certain part of video
				pTime.valueProperty().addListener((ov) -> {
					if (pTime.isPressed()) { // It would set the time
						// as specified by user by pressing
						player.seek(player.getMedia().getDuration().multiply(pTime.getValue() / 100));
					}
				});

				// providing functionality to volume slider
				pVol.valueProperty().addListener((ov) -> {
					if (pVol.isPressed()) {
						player.setVolume(pVol.getValue() / 100); // It would set the volume
						// as specified by user by pressing
					}
				});
				// player.play();
				navBox.setVisible(true);
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}

	public void exitApp(ActionEvent evt) {
		System.exit(0);
	}

	public void about(ActionEvent evt) {
		try {
			System.out.println("About the APP!");
			Window parent = mAbout.getParentPopup().getScene().getWindow();
			Stage aboutStage = new Stage();
			Pane root;
			root = (Pane) FXMLLoader.load(getClass().getResource("about.fxml"));
			Scene scene = new Scene(root, 300, 300);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			// Specifies the owner Window (parent) for new window
			aboutStage.initOwner(parent);
			// Specifies the modality for new window.
			aboutStage.initModality(Modality.WINDOW_MODAL);
			aboutStage.setScene(scene);
			aboutStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void closeAbout(ActionEvent evt) {
		Button source = (Button) evt.getSource();
		Window parent = source.getScene().getWindow();
		parent.hide();
	}

	public void myalert(ActionEvent evt) {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("This is a test");
		// alert.setHeaderText("I am the head");
		alert.setHeaderText(null);
		alert.setContentText("I am the content");
		alert.showAndWait();
	}

	public void playMovie(ActionEvent evt) {
		if (player == null) return;
		System.out.println("Play button clicked. player status is "+ player.getStatus());
		
		if (MediaPlayer.Status.PLAYING == player.getStatus()) {
			player.pause();
			btnPlay.setText(">");
		} else {
			player.play();
			btnPlay.setText("||");
		}
	}

	private void updatesValues() {
		Platform.runLater(() -> {
			// Updating to the new time value
			// This will move the slider while running your video
			if (player != null) {
				pTime.setValue(player.getCurrentTime().toMillis() / player.getTotalDuration().toMillis() * 100);
			}
		});
	}
}
