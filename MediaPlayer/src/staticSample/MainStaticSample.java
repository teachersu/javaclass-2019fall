package staticSample;

public class MainStaticSample {

	public static void main(String[] args) {
		ReadCounter reader1 = new ReadCounter();
		ReadCounter reader2 = new ReadCounter();
		Sample1 s1 = new Sample1();
		Sample1 s2 = new Sample1();
		accessSample(s1, "reader one");
		accessSample(s2, "reader two");
		reader1.modify(10);
		accessSample(s1, "reader one");
		accessSample(s2, "reader two");
		s1.value = 100;
		s2.value = 200;
		accessSample(s1, "reader one");
		accessSample(s2, "reader two");
		reader2.modify(4000);
		accessSample(s1, "reader one");
		accessSample(s2, "reader two");
	}
	
	public static void accessSample(Sample1 s, String reader) {
		System.out.println(reader + " gets values: Count=" + Sample1.count);
		System.out.println(reader + " gets values: Value=" + s.value);
		
	}

}
